<?php


DB::listen(function ($query){
    //echo "<pre>{{$query->time}}</pre>";
    //echo "<pre>{{$query->sql}}</pre>";
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    //return ['uno'=>'1','dos'=>'2'];
});

Route::get('saludo/{nombre?}', 'PrincipalController@saludo')->name('saludo')->middleware('verified');

Route::get('admin/lista', 'PrincipalController@lista')->name('lista');

Route::get('condicional', 'PrincipalController@condicional')->name('condicional');






/*########################## MENSAJES ###########################*/
/*Route::get('mensaje', 'MensajesController@index')->name('mensaje.index');
Route::get('mensaje/create', 'MensajesController@create')->name('mensaje.create');
Route::post('mensaje/store','MensajesController@store')->name('mensaje.store');
Route::get('mensaje/{id}','MensajesController@show')->name('mensaje.show');
Route::get('mensaje/{id}/edit','MensajesController@edit')->name('mensaje.edit');
Route::put('mensaje/{id}','MensajesController@update')->name('mensaje.update');
Route::delete('mensaje/{id}','MensajesController@destroy')->name('mensaje.destroy');*/

Route::resource('mensaje','MensajesController');


Route::resource('usuario','UsuariosController')->except('show');







/*Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');*/




Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
