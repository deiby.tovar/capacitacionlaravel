<div class="card">
    <div class="card-header">
        <h3 class="card-title">Formulario de contacto</h3>
    </div>
    <div class="card-body">
        @guest
        <div class="form-group">
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $mensaje->nombre)}}">
            {{$errors->first('nombre')}}
        </div>
        <div class="form-group">
            <label for="nombre">email:</label>
            <input type="text" name="email" id="email" class="form-control" value="{{old('email', $mensaje->email)}}">
            {{$errors->first('email')}}
        </div>
        @endguest
        <div class="form-group">
            <label for="body">Mensaje:</label>
            <textarea name="body" id="body" class="form-control">{{old('body', $mensaje->mensaje)}}</textarea>
            {{$errors->first('body')}}
        </div>
    </div>
    <div class="card-footer">
        <button class="btn btn-primary">Enviar</button>
    </div>
</div>