@extends('layouts.app')


@section('content')

    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
    @endif

    <div class="row justify-content-center mt-3">
        <div class="col-md-8 col-12">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">mensaje de {{$mensaje->nombre}}</h3>
                    </div>
                    <div class="card-body">
                        <p>Nombre: {{$mensaje->nombre}}</p>
                        <p>Email: {{$mensaje->email}}</p>
                        <p>MEnsaje: {{$mensaje->mensaje}}</p>
                </div>

        </div>

    </div>

@stop
