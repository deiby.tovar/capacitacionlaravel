@extends('layouts.app')


@section('content')

    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
    @endif

    <div class="row justify-content-center mt-3">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Lista de mensajes</h3>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead class="bg-dark text-white">
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Mensaje</th>
                            <th>Nota</th>
                            <th>Etiquetas</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mensajes as $mensaje)
                            <tr>
                                <td>{{$mensaje->id}}</td>
                                <td>{{$mensaje->user_id?$mensaje->user->name:$mensaje->nombre}}</td>
                                <td>{{$mensaje->user_id?$mensaje->user->email:$mensaje->email}}</td>
                                <td>{{$mensaje->mensaje}}</td>
                                <td>{{$mensaje->nota?$mensaje->nota->body:''}}</td>
                                <td>
                                    @foreach($mensaje->tags as $tag)
                                        {{$tag->nombre}}
                                    @endforeach
                                </td>
                                <td>
                                    <a
                                            class="btn btn-info btn-sm text-white"
                                            href="{{route('mensaje.show',$mensaje->id)}}"
                                            data-toggle="tooltip" data-placement="top"
                                            title="Ver">
                                        <li class="fa fa-eye"></li>
                                    </a>
                                    <a
                                            class="btn btn-info btn-sm text-white"
                                            href="{{route('mensaje.edit',$mensaje->id)}}"
                                            data-toggle="tooltip" data-placement="top"
                                            title="Editar">
                                        <li class="fa fa-edit"></li>
                                    </a>
                                    <form style="display: inline-block" method="post"
                                          action="{{route('mensaje.destroy', $mensaje->id)}}">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" onclick="return confirm('Esta segudo que desea eliminar')"
                                                class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top"
                                                title="Eliminar">
                                            <li class="fa fa-trash"></li>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="row justify-content-center">
                        {{$mensajes->links()}}
                    </div>

                </div>
            </div>
        </div>

    </div>

@stop

@section('scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection



