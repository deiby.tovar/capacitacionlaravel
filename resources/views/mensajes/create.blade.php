@extends('layouts.app')


@section('content')

    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
    @endif

    <div class="row justify-content-center mt-3">
        <div class="col-md-8 col-12">
            <form action="{{route('mensaje.store')}}" method="post">
                @csrf
                @include('mensajes.form_mensaje',['mensaje'=> new \App\Models\Mensaje()])
            </form>
        </div>

    </div>

@stop
