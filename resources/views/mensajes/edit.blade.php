@extends('layouts.app')


@section('content')

    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
    @endif

    <div class="row justify-content-center mt-3">
        <div class="col-md-8 col-12">
            <form action="{{route('mensaje.update', $mensaje->id)}}" method="post">
                @csrf
                @method('PUT')
                @include('mensajes.form_mensaje')
            </form>
        </div>

    </div>

@stop

