@extends('layouts.app')


@section('content')

    @if(empty($lista))
        <p>No hay elementos para mostrar</p>
    @else
        <ul>
            @foreach($lista as $item)
                <li>{{$item}}</li>
            @endforeach
        </ul>
    @endif
@stop