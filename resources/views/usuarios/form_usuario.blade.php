

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Información usuario</h3>
    </div>
    <div class="card-body">
        <div class="form-group">
            <label for="nombre">Name:</label>
            <input type="text" name="name" id="name" class="form-control"
                   value="{{old('name', $usuario->name)}}">
            {{$errors->first('nombre')}}
        </div>
        <div class="form-group">
            <label for="email">email:</label>
            <input type="text" name="email" id="email" class="form-control"
                   value="{{old('email', $usuario->email)}}">
            {{$errors->first('email')}}
        </div>
        @if(auth()->user()->hasRole(['admin']))
            <div class="form-group">
                <label for="email" class="d-block">Roles:</label>
                @foreach($roles as $role)
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox"
                               {{$usuario->roles->contains($role)?'checked':''}}  name="role[]"
                               value="{{$role->id}}">
                        <label class="form-check-label">{{$role->display_name}}</label>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
    <div class="card-footer">
        <button class="btn btn-primary">Enviar</button>
    </div>
</div>
