@extends('layouts.app')


@section('content')

    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
    @endif

    <div class="row justify-content-center mt-3">
        <div class="col-12">

            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h3 class="card-title">Lista de usuarios</h3>
                    <a href="{{route('usuario.create')}}" class="btn btn-primary">Nuevo usuario</a>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead class="bg-dark text-white">
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Rol</th>
                            <th>Nota</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($usuarios as $usario)
                            <tr>
                                <td>{{$usario->id}}</td>
                                <td>{{$usario->name}}</td>
                                <td>{{$usario->email}}</td>
                                <td>{{$usario->roles->pluck('display_name')->implode(' - ')}}</td>
                                <td>{{$usario->nota?$usario->nota->body:''}}</td>
                                <td>
                                    <a
                                            class="btn btn-info btn-sm text-white"
                                            href="{{route('usuario.edit',$usario->id)}}"
                                            data-toggle="tooltip" data-placement="top"
                                            title="Editar">
                                        <li class="fa fa-edit"></li>
                                    </a>
                                    <form style="display: inline-block" method="post"
                                          action="{{route('usuario.destroy', $usario->id)}}">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" onclick="return confirm('Esta seguro que desea eliminar')"
                                                class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top"
                                                title="Eliminar">
                                            <li class="fa fa-trash"></li>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$usuarios->links()}}
                </div>
            </div>
        </div>

    </div>

@stop

@section('scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection



