/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});


Echo.channel('recibir-mensaje')
    .listen('AutorespondedorEvent', (data) => {
        let mensaje = data.mensaje;
        html = `<tr>
                                <td>${mensaje.id}</td>
                                <td>${mensaje.nombre}</td>
                                <td>${mensaje.email}</td>
                                <td>${mensaje.mensaje}</td>
                                <td></td>
                                <td>
                                  
                                </td>
                                <td>
                                    <a
                                            class="btn btn-info btn-sm text-white"
                                            href="/mensaje/${mensaje.id}"
                                            data-toggle="tooltip" data-placement="top"
                                            title="Ver">
                                        <li class="fa fa-eye"></li>
                                    </a>
                                    <a
                                            class="btn btn-info btn-sm text-white"
                                            href="/mensaje/${mensaje.id}/edit"
                                            data-toggle="tooltip" data-placement="top"
                                            title="Editar">
                                        <li class="fa fa-edit"></li>
                                    </a>
                                    <form style="display: inline-block" method="post"
                                          action="/mensaje/${mensaje.id}">
                                        <button type="submit" onclick="return confirm('Esta segudo que desea eliminar')"
                                                class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top"
                                                title="Eliminar">
                                            <li class="fa fa-trash"></li>
                                        </button>
                                    </form>
                                </td>
                            </tr>`;

        $('tbody').prepend(html);
    });
