<?php

namespace App\Http\Controllers;

use App\Http\Requests\MensajeRequest;
use App\Models\Mensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrincipalController extends Controller
{



    public function saludo($nombre = "jose"){

        return view('saludo', compact('nombre'));
    }


    public function lista(){
        $lista = [
          'luisa',
          'jorge',
          'nataly',
          'cristian',
          'gabriel'
        ];

        return view('lista', compact('lista'));
    }


    public function condicional()
    {
        $lista = [
            'luisa',
            'jorge',
            'nataly',
            'cristian',
            'gabriel'
        ];

        return view('condicionales', compact('lista'));

    }

    public function contacto(){
        return view('contacto');
    }



    public function multimetodo(){
        if(request()->isMethod('get')){
            return 'entre por get';
        }elseif (request()->isMethod('post')){
            return 'entre por post';
        }
    }
}
