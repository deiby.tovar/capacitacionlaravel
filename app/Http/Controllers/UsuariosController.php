<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Mensaje;
use App\Models\Role;
use App\User;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UsuariosController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin')->except('edit', 'update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $key = request()->query('page',1);
        $usuarios = Cache::tags('usuarios')->rememberForever('usuarios'.$key, function () {
            return User::with('roles', 'nota')->paginate(10);
        });
        return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::query()->select('id', 'display_name')->get();
        return view('usuarios.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = new User($request->all());
        $usuario->password = bcrypt('password');
        $usuario->save();
        $usuario->roles()->attach($request->role);
        Cache::tags('usuarios')->flush();
        return redirect()->route('usuario.index')->with('success','El usuario fue creado');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Cache::tags('usuarios')->rememberForever('usuarios.show'.$id, function () use ($id) {
            return $usuario = User::query()->findOrFail($id);
        });
        $this->authorize('view', $usuario);

        return view('usuarios.edit', [
            'usuario' => $usuario,
            'roles' => Role::query()->select('id', 'display_name')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $usuario = User::query()->findOrFail($id);
        $this->authorize('view', $usuario);
        $usuario->update($request->all());
        if(auth()->user()->hasRole(['admin'])){
            $usuario->roles()->sync($request->role);
        }
        Cache::tags('usuarios')->flush();
        return redirect()->back()->with('success', 'El usuario sido actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::query()->findOrFail($id);
        $usuario->delete();
        $usuario->roles()->detach();
        Cache::tags('usuarios')->flush();
        return redirect()->route('usuario.index')->with('success', 'El usuario sido eliminado');
    }
}
