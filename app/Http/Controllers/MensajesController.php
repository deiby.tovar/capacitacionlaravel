<?php

namespace App\Http\Controllers;

use App\Events\AutorespondedorEvent;
use App\Http\Requests\MensajeRequest;
use App\Mail\NotificarAlAdministradorMail;
use App\Mail\NotificarAlUsuarioMail;
use App\Models\Mensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MensajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $key = request()->query('page',1);

        $mensajes = \Cache::tags('mensajes')->rememberForever('mensajes'.$key, function () {
             return Mensaje::with('user','nota','tags')->paginate(10);
        });


        return view('mensajes.index', compact('mensajes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mensajes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MensajeRequest $request)
    {
        $mensaje = new Mensaje($request->all());
        $mensaje->mensaje = $request->body;
        $mensaje->save();
        \Cache::tags('mensajes')->flush();
        if(auth()->check()){
            auth()->user()->mensajes()->save($mensaje);
        }

        event(new AutorespondedorEvent($mensaje));

       /* Mail::send(new NotificarAlAdministradorMail($mensaje));
        Mail::send(new NotificarAlUsuarioMail($mensaje));*/

        return redirect()->route('mensaje.index')->with('success','El mensaje fue creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $mensaje = \Cache::tags('mensajes')->rememberForever('mensaje.show'.$id, function () use ($id) {
            return $mensaje = Mensaje::query()->findOrFail($id);
        });

        return view('mensajes.show', compact('mensaje'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mensaje = \Cache::tags('mensajes')->rememberForever('mensaje.show'.$id, function () use ($id) {
            return $mensaje = Mensaje::query()->findOrFail($id);
        });
        return view('mensajes.edit', compact('mensaje'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MensajeRequest $request, $id)
    {
        $mensaje = Mensaje::query()->findOrFail($id);
        $mensaje->fill($request->all());
        $mensaje->mensaje = $request->body;

        $mensaje->save();
        \Cache::tags('mensajes')->flush();
        return redirect()->route('mensaje.index')->with('success','El mensaje fue actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mensaje = Mensaje::query()->findOrFail($id);
        $mensaje->delete();
        \Cache::tags('mensajes')->flush();
        return redirect()->route('mensaje.index')->with('success','El mensaje fue eliminadoº');
    }
}
