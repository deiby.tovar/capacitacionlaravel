<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MensajeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('post')){
            return [
                'nombre' => '',
                'email' => 'email|unique:mensajes',
                'body' => 'required|max:250'
            ];
        }

        if($this->isMethod('put')){
            return [
                'nombre' => '',
                'email' => 'email|unique:mensajes,email,'.$this->mensaje,
                'body' => 'required|max:250'
            ];
        }

    }


    public function attributes()
    {
        return [
           'body' => 'Mensaje'
        ];
    }
}
