<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'nombre'
    ];


    public function mensajes()
    {
        return $this->morphedByMany(Mensaje::class, 'taggable','taggables');
    }


    public function users()
    {
        return $this->morphedByMany(User::class, 'taggable','taggables');
    }
}
