<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{

    protected $fillable = [
        'nombre',
        'email',
        'mensaje'
    ];

    /**
     *
     * un mensjae pertenece a un usuario
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function nota()
    {
        return $this->morphOne(Nota::class, 'notable');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable','taggables');
    }
}
