<?php

namespace App\Listeners;

use App\Events\AutorespondedorEvent;
use App\Mail\NotificarAlUsuarioMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class NotificarUsuarioListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AutorespondedorEvent  $event
     * @return void
     */
    public function handle(AutorespondedorEvent $event)
    {
        Mail::send(new NotificarAlUsuarioMail($event->mensaje));
    }
}
