<?php

namespace App;

use App\Models\Mensaje;
use App\Models\Nota;
use App\Models\Role;
use App\Models\Tag;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * un usuario tiene un rol (relacion de uno a uno)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    /**
     * Metodo que me determina si un usuario tiene determinado rol
     * @param $roles es una array de roles
     * @return bool
     */
    public function hasRole($roles){
        return $this->roles->pluck('name')->intersect($roles)->count();
    }

    /**
     * un usuario tiene muchos mensajes (relacion de uno a muchos)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensajes(){
        return $this->hasMany(Mensaje::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function nota()
    {
        return $this->morphOne(Nota::class, 'notable');
    }


    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable','taggables');
    }
}
