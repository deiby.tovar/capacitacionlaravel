<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificarAlUsuarioMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mensaje;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensaje)
    {
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(!empty($this->mensaje->user_id)){
            $email = $this->mensaje->user->email;
        }else{
            $email = $this->mensaje->email;
        }
        return $this->to($email)
            ->subject('Se ha enviado un mensaje')
            ->view('mails.notificaradmin');
    }
}
