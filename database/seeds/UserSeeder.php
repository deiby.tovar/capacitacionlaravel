<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\User::query()->create([
            'name' => 'deiby',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123'),
        ]);

        factory(\App\User::class)->create([
           'email' => 'jorge@gmail.com',
        ]);

        factory(\App\User::class,10)->create();

        $admin = \App\Models\Role::query()->create([
            'name' => 'admin',
            'display_name' => 'Administrador',
        ]);

        $moderador = \App\Models\Role::query()->create([
            'name' => 'mod',
            'display_name' => 'Moderador',
        ]);

        $estudiante = \App\Models\Role::query()->create([
            'name' => 'Est',
            'display_name' => 'Estudiante',
        ]);

        \App\Models\Tag::query()->create([
            'nombre' => 'importante',
        ]);

        \App\Models\Tag::query()->create([
            'nombre' => 'no importante',
        ]);

        factory(\App\Models\Mensaje::class,10)->create();
    }
}
