<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Mensaje::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'nombre' => $faker->name,
        'mensaje' => $faker->sentence,
        'user_id' => factory(\App\User::class)->create()
    ];
});
