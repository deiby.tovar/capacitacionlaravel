<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MensajesTest extends TestCase
{
    use  RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
       // $this->withoutExceptionHandling();
        $mensaje = [
            'nombre' => 'deiby',
            'email' => 'admin@gmail.com',
            'body' => 'mi primer mensaje',
            '_token' => csrf_token()
        ];

        $response = $this->post(route('mensaje.store'), $mensaje);

        $response->assertRedirect(route('mensaje.index'));

        $response->assertSessionHas('success');

        $this->assertDatabaseHas('mensajes',[
           'mensaje' => 'mi primer mensaje',
            'nombre' => 'deiby',
            'email' => 'admin@gmail.com',
        ]);
    }
}
