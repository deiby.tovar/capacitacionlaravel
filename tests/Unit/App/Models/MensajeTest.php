<?php

namespace Tests\Unit\App\Models;

use App\Models\Mensaje;
use App\Models\Nota;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MensajeTest extends TestCase
{

    use RefreshDatabase;
    /**
     * Se prueba relacion de mensaje con usuario.
     *
     * @test
     */
    public function un_mensaje_pertenece_a_un_usuario()
    {
        $mensaje = factory(Mensaje::class)->create();
        $this->assertInstanceOf(User::class, $mensaje->user);
    }

    /**
     * @test
     */
    public function un_mensaje_tiene_una_nota(){

        $mensaje = factory(Mensaje::class)->create();
        $mensaje->nota()->create([
           'body' => 'esta es una nota'
        ]);

        $this->assertInstanceOf(Nota::class, $mensaje->nota);
    }
}
